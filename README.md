# Apertis statistics

This repository is meant to collect and render a few statistics about Apertis over time.

See https://infrastructure.pages.apertis.org/stats/ for the latest rendered page. 